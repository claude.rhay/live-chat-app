# LIVE CHAT APP

## Description

An open group live chat application that can do a realtime messaging chat with other users connected to the server.

## Built using these technologies

- [NodeJs](https://nodejs.org/en/)
- [ReactJs](https://reactjs.org/)
- [Socket.io](https://socket.io/)
- [Tailwind CSS](https://tailwindcss.com/)

## Live Demo

http://live-chat-app-claude.herokuapp.com/

## Github Repo

https://gitlab.com/claude.rhay/live-chat-app
