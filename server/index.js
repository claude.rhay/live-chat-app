const express = require('express');
const path = require('path');

const app = express();
const server = require('http').createServer(app);
const cors = require('cors');

// configure socket.io to allow cross-origin requests
const io = require('socket.io')(server, { cors: { origin: '*' } });

// set up our PORT
const MY_PREFERRED_PORT = 8020;
const PORT = process.env.PORT || MY_PREFERRED_PORT;

// allow express to open react-app
app.use(express.static(path.join(`${__dirname}/public`)));

// allow cors
app.use(cors());

// on socket connection
io.on('connection', (socket) => {
  console.log(`User Connected: ${[socket.id]}`);
  // get active or connected socket
  socket.emit('activeUsers');

  // join chat room
  socket.on('join_chat', () => {
    socket.join();
    console.log(`User with ID: ${socket.id} joined chat`);
  });

  // get socket id of connected socket
  socket.emit('getId', socket.id);

  // when a chat event is emitted
  socket.on('chat', (id, chat) => {
    io.emit('sendChat', id, chat, socket.id);
  });

  // when a user changes their username
  socket.on('usernameChange', (username, socketid) => {
    socket.broadcast.emit('resetChat', username, socketid);
  });

  // when a user is Typing
  socket.on('userTyping', (socketid, type) => {
    socket.broadcast.emit('someoneTyping', socket.id, type);
  });

  // get all connected sockets
  socket.on('activeUsers', () => {
    // gets all connected sockets
    const onlineUsers = io.engine.clientsCount;
    socket.emit('countUsers', onlineUsers);
    // console.log(`No. of users connected: ${onlineUsers}`);
  });

  // when a socket gets disconnected
  socket.on('disconnect', () => {
    console.log('User Disconnected', socket.id);
    socket.broadcast.emit('disconnectNotification', socket.id);
    const onlineUsers = io.engine.clientsCount;
    socket.broadcast.emit('countUsers', onlineUsers);
    // console.log(`No. of users connected: ${onlineUsers}`);
  });
});

server.listen(PORT, () => {
  console.log(`Application running successfully on port: ${PORT}`);
  console.log(process.env.REACT_APP_SERVER_ENDPOINT);
});
