import React, { useState, useEffect, useRef } from 'react';

function Chat({ socket, username, id }) {
  const toastRef = useRef(null);

  const [activeUsers, setActiveUsers] = useState(0);
  const [chat, setChat] = useState('');
  const [, setId] = useState('');
  const [socketid, setSocketId] = useState('');
  const [allChats, setAllChats] = useState([]);
  const [notification, setNotification] = useState('');
  const [someoneTyping, setSomeoneTyping] = useState('');

  // scroll to bottom page function when message is sent
  const scrollToBottom = () => {
    window.scroll({
      top: document.body.offsetHeight, left: 0, behavior: 'smooth',
    });
  };

  // function to show toast
  const showToast = (bgColor) => {
    toastRef.current.className = 'show';
    toastRef.current.style.backgroundColor = bgColor;
    setTimeout(() => {
      toastRef.current.className = toastRef.current.className.replace('show', '');
    }, 3000);
  };

  // function handle chat message input
  const handleChatInput = (e) => {
    setChat(e.target.value);
  };

  // function to submit chat
  const submitChat = async () => {
    if (!chat) return;
    socket.emit('chat', id, chat, socketid);
    setChat('');
    scrollToBottom();
  };

  // function to disconnect socket
  const logOut = () => {
    socket.disconnect();
    window.location.reload(false);
  };

  // function to get time when message or chat was sent
  const getTime = () => {
    const time = new Date(); const hour = time.getHours(); const minutes = time.getMinutes();
    const meridiem = `${time.getHours() > 12 ? 'PM' : 'AM'}`;
    return `${hour}:${minutes} ${meridiem}`;
  };

  // function to search user
  const searchUser = (userId) => {
    let user = {};
    for (let i = 0; i < allChats.length; i += 1) {
      if (allChats[i].socketid === userId) {
        user = allChats[i];
      }
    }
    return user;
  };

  // function to handle when a user or socket is typing
  const handleUserTyping = () => {
    clearTimeout(timeout);
    socket.emit('userTyping', socketid, 'start');
    var timeout = setTimeout(() => { socket.emit('userTyping', socketid, 'stop'); }, 1000);
  };

  /*
      First useEffect: runs only once and
      attaches the events below to a connected socket instance
    */
  useEffect(() => {
    // connect with server
    const newSocket = socket;

    // if connection error
    newSocket.on('connect_error', () => {
      alert('Sorry, there seems to be an issue with the connection! Try refreshing');
      setNotification('Logout Successful!\n Refresh page to reconnect');
      showToast('green');
    });

    // get all users or sockets connected
    newSocket.emit('activeUsers');

    // gets socket id of a socket or user
    newSocket.on('getId', (idOfSocket) => {
      setId(idOfSocket);
      setSocketId(idOfSocket);
    });

    // gets the number of connects
    newSocket.on('countUsers', (onlineUsers) => {
      setActiveUsers(onlineUsers);
    });

    // adds a message to the list of messages or chats
    // and scrolls to bottom of page
    newSocket.on('sendChat', (userId, userChat, idOfSocket) => {
      setAllChats((prevArr) => [...prevArr, {
        chat: userChat,
        id: userId,
        socketid: idOfSocket,
        time: getTime(),
      }]);
      scrollToBottom();
    });

    // reset all chat when a username is changed
    newSocket.on('resetChat', (nameofUser, usersocketid) => {
      setAllChats((prev) => prev.map(
        (obj) => (obj.socketid === usersocketid ? { ...obj, id: nameofUser } : obj),
      ));
    });

    // triggers when there is a notification
    newSocket.on('message', (message) => {
      setNotification(message);
      showToast('green');
    });

    // clean up to close instance and to avoid side effects
    return () => {
      newSocket.close();
    };
  }, []);

  /*
        Second useEffect: runs when there is a new chat or message
        */
  useEffect(() => {
    // gets connected users
    socket?.emit('activeUsers');

    // a notification for disconnection
    socket?.on('disconnectNotification', (idOfSocket) => {
      const disconnectedUser = searchUser(idOfSocket);
      if (!disconnectedUser || allChats.length === 0) return;
      setNotification(`${disconnectedUser.id} is disconnected!`);
      showToast('red');
    });

    // event for when a socket or user is typing
    socket?.on('someoneTyping', (idOfSocket, type) => {
      if (type === 'stop') {
        setSomeoneTyping('');
      } else {
        const user = searchUser(idOfSocket);
        setSomeoneTyping(user?.id);
      }
    });
  }, [allChats]);

  return (
    <div>
      {/* CHAT BOX */}
      <div className="">

        {/* TOAST NOTIFICATION */}
        <p id="toast" ref={toastRef}>{notification}</p>

        {/* CHAT HEADER */}
        <div className="chat-header fixed p-3 w-full text-center bg-gray-200">
          <span>
            Number of people in Chat:
            <span className="text-center ml-1 text-base text-green-600">{activeUsers}</span>
          </span>
          <br />
          <span>
            Welcome to the chat,
            <h2 className="inline font-extrabold italic">
              {' '}
              {username}
              !
            </h2>
          </span>
          <button type="button" onClick={logOut} className="ml-5 p-3 shadow-lg text-red-500 rounded bg-gray-100">Logout</button>
        </div>

        {/* MESSAGES OR CHATS */}
        <div className="chat-body p-12 md:mx-20 lg:mx-40 mb-40">
          <div className="chat-box mt-40">
            {allChats.map((userChat, i) => (
              <div key={i} className={`${userChat.id === id ? 'mr-0 ml-auto right' : 'left'}  my-3 w-fit shadow-lg p-4 rounded-lg`}>
                <span className={`${userChat.id === id ? '  text-green-500' : 'text-red-500'} rounded font-extrabold`}>{userChat.id}</span>
                <p className="md:text-base">{userChat.chat}</p>
                <span className="text-sm text-gray-400">{userChat.time}</span>
              </div>
            ))}
          </div>
        </div>

        {/* KEYBOARD AREA */}
        <div className="chat-footer fixed z-50 w-full bottom-0 bg-white p-2">
          {someoneTyping && (
          <p className="text-center">
            {someoneTyping}
            {' '}
            is typing...
          </p>
          )}

          <form className="flex flex-row justify-center md:w-1/2 md:m-auto" onSubmit={(e) => { e.preventDefault(); submitChat(chat); }}>
            <input
              value={chat}
              onKeyUp={() => { setSomeoneTyping(null); }}
              onKeyDown={handleUserTyping}
              onChange={(e) => { handleChatInput(e); }}
              onKeyPress={(e) => { if (e.key === 'Enter') handleChatInput(e); }}
              className="border md:w-3/4 p-4 focus:outline-green-500"
              placeholder="Type in your message"
            />
            <button className="p-3 shadow-lg bg-gray-100 rounded text-green-500" type="submit">Send</button>
          </form>
          <p className="text-center">
            Made
            by
            {' '}
            <a href="https://www.linkedin.com/in/clauderhay/" className="underline">Claude</a>
          </p>
        </div>
      </div>
    </div>
  );
}

export default Chat;
