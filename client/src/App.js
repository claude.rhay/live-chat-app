import React, {
  useState, useRef, useEffect,
} from 'react';
import { io } from 'socket.io-client';
import './App.css';
import Chat from './Chat';

function App() {
  // set up toast as ref
  const toastRef = useRef(null);

  // create states
  const [activeUsers, setActiveUsers] = useState(0);
  const [socket, setSocket] = useState(null);
  const [id, setId] = useState('');
  const [socketid, setSocketId] = useState('');
  const [username, setUsername] = useState('');
  const [notification, setNotification] = useState('');
  const [showChat, setShowChat] = useState(false);

  // function to join the chatroom and change the username
  const joinChat = () => {
    if (username !== '') {
      socket.emit('join_chat');
      setShowChat(true);
      setId(username);
      socket.emit('usernameChange', username, socketid);
    } else {
      setNotification('Please enter a username to continue.');
      showToast('red');
    }
  };

  // function to show toast
  const showToast = (bgColor) => {
    toastRef.current.className = 'show';
    toastRef.current.style.backgroundColor = bgColor;
    setTimeout(() => {
      toastRef.current.className = toastRef.current.className.replace('show', '');
    }, 3000);
  };

  useEffect(() => {
    const newSocket = io(process.env.REACT_APP_SERVER_ENDPOINT);
    setSocket(newSocket);

    // if connection error
    newSocket.on('connect_error', (err) => {
      console.log(`Connection Error due to : ${err.message}`);
    });

    // get all users or sockets connected
    newSocket.emit('activeUsers');

    // gets socket id of a socket or user
    newSocket.on('getId', (idOfSocket) => {
      setId(idOfSocket);
      setSocketId(idOfSocket);
    });

    // gets the number of connects
    newSocket.on('countUsers', (onlineUsers) => {
      setActiveUsers(onlineUsers);
    });
  }, []);

  return (
    <div className="App">
      {!showChat ? (
        <div className="fixed p-3 w-full text-center bg-gray-200">
          <span>
            Number of people in Chat:
            <span className="text-center ml-1 text-base text-green-600">{activeUsers}</span>
          </span>
          <div>
            <h4 className="text-center">
              Username:
              <span className="text-green-600 font-extrabold rounded-full">
                {id}
                {' '}
              </span>
            </h4>
          </div>
          <div className="transition-all duration-1000 text-center">
            <label className="block text-gray-700">Change your Username:</label>
            <div className="flex flex-row items-center justify-center">
              <input type="text" onChange={(e) => { setUsername(e.target.value); }} value={username} className="border rounded p-2 focus:outline-green-500 text-gray-400" placeholder="Change Username..." />
              <button type="button" onClick={joinChat} className="p-3 shadow-lg bg-gray-100 rounded  ml-3 text-green-500">Join Chat</button>
            </div>
          </div>
          {/* TOAST NOTIFICATION */}
          <p id="toast" ref={toastRef}>{notification}</p>
        </div>
      ) : (
        <Chat socket={socket} username={username} id={id} />
      )}
    </div>
  );
}

export default App;
